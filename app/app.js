Ext.Loader.setConfig({enabled: true});

Ext.application({
    requires: ['Ext.container.Viewport',
        'Ext.example.msg',
        'Ext.grid.plugin.BufferedRenderer'
    ],
    name: 'SPONGE',
    appFolder: 'app',
    launch: function () {

        Ext.create('SPONGE.view.MainView');

        application = this;

    },

    controllers: ['MainView','CreateAnalysisPanel', 'ResultsPanel']


});
