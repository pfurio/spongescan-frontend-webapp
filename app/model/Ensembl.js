Ext.define('SPONGE.model.Ensembl', {
    extend: 'Ext.data.Model',
    fields: [
        { name: 'name', type: 'string' },
        { name: 'value', type: 'string' }
    ]
});

