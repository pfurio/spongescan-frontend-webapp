// ResultsPanel controller

Ext.define('SPONGE.controller.ResultsPanel', {
    extend: 'Ext.app.Controller',
    views: [
        'ResultsPanel'
    ],
    stores: [
        'GridStore'
    ],
    models: [
        'GridModel'
    ],
    init: function () {
        this.control({
            'combobox[name="kmer_combo"]': {
                change: function (thiss, newValue) {
                    var miGrid = Ext.getCmp("resultsGrid");
                    var mistore = miGrid.getStore();
                    // Disable toolbars
                    myfilters.setDisabled(true);
                    Ext.getCmp('filterBar').setDisabled(true);
                    mistore.loadPage(1, {"url": "webserver/getJob/" + jobID + "/" + newValue + "/" + (thiss.next().getValue() == true ? "1" : "0")});

                }
            },
            'checkbox[name="mirna_checkbox"]': {
                change: function (thiss, newValue) {
                    var miGrid = Ext.getCmp("resultsGrid");
                    var mistore = miGrid.getStore();
                    // Disable toolbars
                    myfilters.setDisabled(true);
                    Ext.getCmp('filterBar').setDisabled(true);
                    mistore.loadPage(1, {"url": "webserver/getJob/" + jobID + "/" + thiss.previousNode().getValue() + "/" + (newValue == true ? "1" : "0")});
                }
            },
            'grid tableview': {
                expandbody: function (rowNode, record, expandRow) {
                    tpl_rowexpand.overwrite(expandRow.firstChild.firstChild, record.data);
                },
                itemdblclick: function (thiss, record) {
                    this.createExpressionWindow(record.data);
                }
            },
            'grid': {
                columnshow: function(ct, column) {
                    var selectedFields = Ext.getCmp("selectedfields").items.items;

                    for (var i = 0; i < selectedFields.length; i++) {
                        if (selectedFields[i].name === column.dataIndex) {
                            selectedFields[i].setValue(true);
                            break;
                        }
                    }
                },
                columnhide: function(ct, column) {
                    var selectedFields = Ext.getCmp("selectedfields").items.items;

                    for (var i = 0; i < selectedFields.length; i++) {
                        if (selectedFields[i].name === column.dataIndex) {
                            selectedFields[i].setValue(false);
                            break;
                        }
                    }
                }
            },
            'button[action=refreshFields]': {
                click: this.refreshFields
            },
            'button[action=addFilter]': {
                click: function () {
                    var filterField = Ext.getCmp('filterField');
                    var filterDisplay = filterField.getDisplayValue();
                    var filterBy = filterField.getValue();
                    var operation = Ext.getCmp("filterOperation").getValue();
                    this.addFilter(filterBy, operation, filterDisplay);
                }
            },
            'button[action=removeFilter]': {
                click: this.removeFilter
            }
        })

    },
    redrawExpandedRows: function () {

        var grid = Ext.getCmp("resultsGrid"),
            rowExpander = grid.plugins[0],
            view = rowExpander.view;

        var expand = typeof expand !== 'undefined' ? expand : true;
        var nodes = rowExpander.view.getNodes();


        for (var i = 0; i < nodes.length; i++) {
            var node = Ext.fly(nodes[i]);

            if (node.hasCls(rowExpander.rowCollapsedCls) != expand) {
                var rowNode = view.getNode(i);
                var row = Ext.fly(rowNode, '_rowExpander');
                var nextBd = row.down(rowExpander.rowBodyTrSelector, true);
                var record = grid.getStore().getAt(i);
                tpl_rowexpand.overwrite(nextBd.firstChild.firstChild, record.data);

            }
        }
    },
    refreshFields: function () {
        // var oldFilters = grid.getStore().filters;

        var selection = Ext.getCmp('selectedfields').getValue();
        var grid = Ext.getCmp("resultsGrid");
        var columns = grid.columns;

        for (var i = 0; i < columns.length; i++) {
            var hide = true;
            if (columns[i].hasOwnProperty("dataIndex")) {
                for (var j in selection) {
                    if (selection[j] === columns[i].dataIndex) {
                        hide = false;
                    }
                }
            }
            columns[i].setVisible(!hide);
        }
        var menu = Ext.getCmp("mainMenu");
        menu.setVisible(false);

        // Draw expanded rows again
        this.redrawExpandedRows();

        // Filter again in case there were filters applied
        /*	debugger

         grid.getStore().addFilter(oldFilters);
         */
    },
    addFilter: function (filterBy, operation, filterDisplay) {

        if (!filterBy)
            Ext.Msg.alert('Warning', 'Filter field is empty');
        else {
            var filterText = Ext.getCmp('filterValue').getValue();
            if (!filterText)
                Ext.Msg.alert('Warning', 'Filter text is empty.');
            else {

                var grid = Ext.getCmp("resultsGrid");
                var column = null;
                var columns = grid.getColumnManager().getColumns();
                for (var i = 0; i < columns.length; i++) {
                    if (columns[i].dataIndex == filterBy) {
                        column = columns[i];
                        break;
                    }
                }

                /* Check if there were any previous filters */
                var removeOper = [""];
                if ((filterBy == "start") || (filterBy == "end") || (filterBy == "trans_start") || (filterBy == "trans_end")
                    || (filterBy == "w_size") || (filterBy == "lod_score") || (filterBy == "compl_score") || (filterBy == "sd")
                    || (filterBy == "occ") || (filterBy == "total") || (filterBy === "length") || (filterBy === "median_distance")) {
                    if (operation == "lt" || operation == "gt") {
                        removeOper = ["eq"];
                    } else if (operation == "eq") {
                        removeOper = ["lt","gt","eq"];
                    }
                } else {
                    operation = "";
                }

                var filterCollection = grid.getStore().getFilters();
                filterCollection.beginUpdate();

                var filterBar = Ext.getCmp('filterBar');
                var toRemoveIndexes = [];
                for (i = 0; i < removeOper.length; i++) {
                    for (var j = 0; j < grid.myFilters.length; j++) {
                        if (grid.myFilters[j].key == filterBy && grid.myFilters[j].operation == removeOper[i]) {
                            /* Remove this filter */
                            toRemoveIndexes.push(j);
                            if (removeOper[i] != "") {
                                var filter = column.filter.filter[removeOper[i]];
                                filter.setValue(null);
                                column.filter.removeStoreFilter(filter);
                            }

                            /* Now remove it from the filterBar */
                            filterBar.remove(filterBy + removeOper[i]);
                            filterBar.remove(filterBy + removeOper[i] + "button");
                        }
                    }
                }

                for (i = 0; i < toRemoveIndexes.length; i++) {
                    grid.myFilters.splice(toRemoveIndexes[i],1);
                }
                var res = true;
                /* Now we try to add the filter */

                if ((filterBy == "start") || (filterBy == "end") || (filterBy == "trans_start") || (filterBy == "trans_end") || (filterBy == "w_size") || (filterBy == "lod_score") ||
                    (filterBy == "compl_score") || (filterBy == "sd") || (filterBy == "occ") || (filterBy == "total") || (filterBy === "length") || (filterBy === "median_distance")) {

                    if (!operation) {
                        Ext.Msg.alert('Warning', 'Select an operator to filter.');
                        res = false;
                    } else {
                        column.filter.filter[operation].setValue(parseFloat(filterText));
                        column.filter.addStoreFilter(column.filter.filter[operation]);
                        filterText = parseFloat(filterText);
                    }
                }
                else {
                    column.filter.setValue(filterText);
                }

                if (res == true) {

                    grid.myFilters.push({"key": filterBy, "value": parseFloat(filterText), "operation": operation, "id": filterBy + operation});

                    if (filterBar == null) {
                        filterBar = Ext.create('Ext.toolbar.Toolbar', {id: 'filterBar', dock: 'top', ui: 'footer'});
                        var gridPanel = Ext.getCmp('resultsGrid');
                        gridPanel.addDocked(filterBar);
                    }

                    var signo = "";
                    if (operation != "")
                        signo = Ext.getCmp("filterOperation").getRawValue();
                    filterBar.add(Ext.create('Ext.Button', {text: filterDisplay + ': ' + signo + filterText.toString(), id: filterBy + operation + "button"}));
                    filterBar.add(Ext.create('Ext.Button', {html: '<i class="fa fa-times"></i>', id: filterBy + operation, action: 'removeFilter'}));
                    filterBar.setVisible(true);

                    Ext.getCmp('filterValue').setValue("");
                    Ext.getCmp("filterOperation").setVisible(false);
                    Ext.getCmp('filterField').clearValue();
                }

                filterCollection.endUpdate();

            }
        }

        this.redrawExpandedRows();
    },
    removeFilter: function (button) {
        var filterField = button.getId();
        var grid = Ext.getCmp("resultsGrid");
        var filterCollection = grid.getStore().getFilters();
        filterCollection.beginUpdate();

        for (var indexGrid = 0; indexGrid < grid.myFilters.length; indexGrid++) {
            if (grid.myFilters[indexGrid].id === filterField)
                break;
        }

        var column = null;
        var columns = grid.getColumnManager().getColumns();
        for (var i = 0; i < columns.length; i++) {
            if (columns[i].dataIndex === grid.myFilters[indexGrid].key) {
                column = columns[i];
                break;
            }
        }

        if (grid.myFilters[indexGrid].operation != "") {
            var filter = column.filter.filter[grid.myFilters[indexGrid].operation];
            filter.setValue(null);
            column.filter.removeStoreFilter(filter);
        } else {
            column.filter.setValue(null);
        }

        /* Now remove it from the filterBar */
        var filterBar = Ext.getCmp('filterBar');
        filterBar.remove(filterField);
        filterBar.remove(filterField + "button");

        filterCollection.endUpdate();

        if (filterBar.items.items.length === 0)
            filterBar.setVisible(false);


        this.redrawExpandedRows();

    },
    createExpressionWindow: function (mydata) {

        // Add lncRNA
        var gene_ids = mydata.gene_id + ",";

        if (mydata.hasOwnProperty("mirnas")) {
            // Add all miRNA ids
            for (var i = 0; i < mydata.mirnas.length; i++) {
                gene_ids = gene_ids + mydata.mirnas[i] + ",";
            }
        }

        gene_ids = gene_ids.substring(0, gene_ids.length - 1);
        // Create expression charts
        $.ajax({
            url: "webserver/getExpression/" + jobID + "/" + gene_ids,
            success: function (data) {

                var myitems = [];

                for (var i = 0; i < data.response.length; i++) {
                    if (data.response[i].found === true) {
			var hasExpression = false;
                        for (var w=0; w < data.response[i].tissueExpr.length; w++) {
                            if (data.response[i].tissueExpr[w].y > 0) {
                                hasExpression = true;
                                break;
                            }
                        }
                        if (hasExpression) {
                            myitems.push({
                                xtype: 'box',
                                expr_values: mydata,
                                mydiv: '#genediv_' + i,
                                gene_id: data.response[i].gene_id,
                                title: data.response[i].gene_id,
                                y_axis: data.response[i].expr_format,
                                tissue_expr: data.response[i].tissueExpr,
                                drilldown_expr: data.response[i].drillDownExpr,
                                html: '<div id="genediv_' + i + '" style="width:800px;"></div>',
                                listeners: {
                                    afterrender: function () {
                                        /* Create expression barplot */
                                        $(this.mydiv).highcharts({
                                            credits: {enabled: false},
                                            chart: {
                                                type: 'column'
                                            },
                                            title: {
                                                text: this.gene_id
                                            },
                                            subtitle: {
                                                text: 'Click the tissues to view the expression of the samples'
                                            },
                                            xAxis: {
                                                type: 'category',
                                                labels: {
                                                    rotation: -45,
                                                    style: {
                                                        fontSize: '10px',
                                                        fontFamily: 'Verdana, sans-serif'
                                                    }
                                                }
                                            },
                                            yAxis: {
                                                title: {
                                                    text: this.y_axis
                                                }
                                            },
                                            legend: {
                                                enabled: false
                                            },
                                            tooltip: {
                                                headerFormat: '',
                                                pointFormat: '<span style="color:{point.color};">{point.name}</span>: <b>{point.y:.2f}</b><br/>'
                                            },
                                            series: [{
                                                name: 'Tissue',
                                                colorByPoint: true,
                                                data: this.tissue_expr
                                            }],
                                            drilldown: {
                                                series: this.drilldown_expr
                                            }
                                        });
                                    }
                                }

                            });                            
                        } else {
                            myitems.push({
                                xtype: 'box',
                                title: data.response[i].gene_id,
				height: 400,
				html: '<div style="text-align: center;font-family: Lato;font-size: x-large;height: 100%;position: relative;top: 50%;">' + data.response[i].gene_id + ' is not expressed.</div>'
				});
			}

                    }
                }
                if (myitems.length > 0) {
                    Ext.create('Ext.window.Window', {
                        title: 'Expression charts',
                        modal: true,
                        height: 500,
                        width: 800,
                        resizable: false,
                        items: {
                            xtype: 'tabpanel',
                            border: false,
                            items: myitems
                        }
                        /*                        listeners: {
                         boxready: function () {
                         this.updateLayout();
                         }
                         }*/
                    }).show();
                } else {
                    Ext.example.msg("Expression data not found.", "");
                }
                /*
                 debugger
                 if (mywindow !== null)
                 mywindow.show();
                 */

            }
            ,
            error: function () {
                console.log("Cannot load expression data.");
            }
        });

        /*

         Barplot

         $(function () {
         $('#container').highcharts({
         chart: {
         type: 'column'
         },
         title: {
         text: gene_id
         },
         xAxis: {
         type: 'samples',
         labels: {
         rotation: -45,
         style: {
         fontSize: '13px',
         fontFamily: 'Verdana, sans-serif'
         }
         }
         },
         yAxis: {
         min: 0,
         title: {
         text: 'Expression'
         }
         },
         legend: {
         enabled: false
         },
         tooltip: {
         pointFormat: 'Expression: <b>{point.y:.1f} </b>'
         },
         series: [{
         name: 'Population',
         data: [
         ['Shanghai', 23.7],
         ['Lagos', 16.1],
         ['Instanbul', 14.2],
         ['Karachi', 14.0],
         ['Mumbai', 12.5],
         ['Moscow', 12.1],
         ['São Paulo', 11.8],
         ['Beijing', 11.7],
         ['Guangzhou', 11.1],
         ['Delhi', 11.1],
         ['Shenzhen', 10.5],
         ['Seoul', 10.4],
         ['Jakarta', 10.0],
         ['Kinshasa', 9.3],
         ['Tianjin', 9.3],
         ['Tokyo', 9.0],
         ['Cairo', 8.9],
         ['Dhaka', 8.9],
         ['Mexico City', 8.9],
         ['Lima', 8.9]
         ],
         dataLabels: {
         enabled: true,
         rotation: -90,
         color: '#FFFFFF',
         align: 'right',
         format: '{point.y:.1f}', // one decimal
         y: 10, // 10 pixels down from the top
         style: {
         fontSize: '13px',
         fontFamily: 'Verdana, sans-serif'
         }
         }
         }]
         });
         });

         */

    }


});

function downloadCSV () {
    var kmers = Ext.getCmp("kmer_combo").getValue();
    var mirnas = Ext.getCmp("mirna_checkbox").getValue() == true ? "1" : "0";
    window.open("http://spongescan.rc.ufl.edu/webserver/download/" + jobID + "/" + kmers + "/" + mirnas);
}

/****************
 *****************
 My templates
 ****************
 ***************/

var tpl_graph = new Ext.XTemplate(
    '<tpl if="cons_graph">',
    '<img src="{cons_graph}" width="100%" height="15">',
    '</tpl>',
//        '<svg id={[values.trans_id + "_" + values.kmer]} width="100%" height="15" viewBox="0 0 100 100" preserveAspectRatio="none"  style="position: absolute; left: 0px; padding: 0px 10px 0px 10px;">',
    '<svg id={[values.trans_id + "_" + values.kmer]} width="100%" height="15" viewBox="0 0 100 100" preserveAspectRatio="none"  ',
    '<tpl if="cons_graph">',
    'style="position: absolute; left: 0px; padding: 0px 10px 0px 10px;"',
    '</tpl>',
    '>',
    '<tpl if!="cons_graph">',
    '<rect x="0" y="0" width="100%" height="100%" style="fill:orange;opacity:0.1"/>',
    '</tpl>',
    '<tpl for="positions">',
    '<a onclick="genomeViewer.setRegion(\'{[this.removeChr(parent.chrom)]}:{[this.getPosInGenome(parent.strand, values, parent.exons)]}\'); $(\'#ResultsPanel\').animate({ scrollTop: $(\'#genomeViewerPanel\').offset().top},400);">',
    '<rect class="binding_site" x="{[this.getPctg(values, parent.trans_end - parent.trans_start , parent.strand)]}" y="0" width="0.15" height="50" style="fill:brown;opacity:0.8"/></a>',
    '</tpl>',
    '</svg>',
    {
        disableFormats: true,
        getPctg: function (pos, length, strand) {
            var posicion = pos;
            if (strand === "-") {
                posicion = length - posicion + 1;
            }
            return (posicion / length) * 100;
        },
        removeChr: function (chromosome) {
            return chromosome.replace('chr', '');
        },
        getPosInGenome: function (strand, pos, exons) {
            if (strand === "+") {
                var tamanyototal = 0;
                for (var i = 0; i < exons.length; i++) {

                    var exonLength = exons[i].end - exons[i].start + 1;
                    if (tamanyototal + exonLength > pos) // El binding está en este exón
                        return (pos - tamanyototal + exons[i].start);
                    tamanyototal += exonLength;
                }
            } else {
                var tamanyototal = 0;
                for (var i = 0; i < exons.length; i++) {

                    var exonLength = exons[i].end - exons[i].start + 1;
                    if (tamanyototal + exonLength > pos) // El binding está en este exón
                        return (exons[i].end - pos + tamanyototal);
                    tamanyototal += exonLength;
                }
            }
            return (1);
        },
    });


var tpl_rowexpand = new Ext.XTemplate(
    '<table cellpadding="2" cellspacing="2" width="100%" border="0">',
    // Binding sites
    '<tr><td width=8%>',
    '<b><small>Binding sites:</small></b>',
    '</td>',
    '<td width=92%>',
    '<svg id={[values.trans_id + "_" + values.kmer]} width=100% height="20" viewBox="0 0 100 100" preserveAspectRatio="none">',
    '<rect x="0" y="0" width="100%" height="100%" style="fill:orange;opacity:0.1"/>',
    '<tpl for="positions">',
    '<a onclick="genomeViewer.setRegion(\'{[this.removeChr(parent.chrom)]}:{[this.getPosInGenome(parent.strand, values, parent.exons)]}\');  $(\'#ResultsPanel\').animate({ scrollTop: $(\'#genomeViewerPanel\').offset().top},400);">',
    '<rect class="binding_site" x="{[this.getPctg(values, parent.trans_end - parent.trans_start , parent.strand)]}" y="0" width="0.15" height="100" style="fill:brown;opacity:0.8"/></a>',
    '</tpl>',
    '</svg>',
    '</td></tr>',
    // Conservation scores
    '<tpl if="cons_graph">',
    '<tr><td width=5%>',
    '<b><small>Conservation:</small></b>',
    '</td>',
    '<td width=95%>',
    '<img src="{cons_graph}" width="100%" height="20" >',
    '</td></tr>',
    '</tpl>',
    // Accessibility
    '<tpl if="acc_graph">',
    '<tr><td width=5%>',
    '<b><small>Accessibility:</small></b>',
    '</td>',
    '<td width=95%>',
    '<img src="{acc_graph}" width="100%" height="20" >',
    '</td></tr>',
    '</tpl>',
    // Exons
    '<tr><td width=5%>',
    '<tpl if="this.isPositiveStrand(values.strand)">',
    '<b><small>Exons (+):</small></b>',
    '<tpl else>',
    '<b><small>Exons (-):</small></b>',
    '</tpl>',
    '</td>',
    '<td width=95%>',
    '<svg id={["exons_" + values.trans_id + "_" + values.kmer]} width=100% height="5" viewBox="0 0 100 100" preserveAspectRatio="none">',
    '<path d="',
    '<tpl for="this.getExonPos(values.exons, 1, values.strand, values.length)">',
    'M {izq} 0 V 100 H {width} V 0 Z ',
    '</tpl>',
    '<tpl if="this.isPositiveStrand(values.strand)">',
    '" fill="green" opacity="0.3"/>',
    '<tpl else>',
    '" fill="red" opacity="0.3"/>',
    '</tpl>',
    '<path d="',
    '<tpl for="this.getExonPos(values.exons, 0, values.strand, values.length)">',
    'M {izq} 0 V 100 H {width} V 0 Z ',
    '</tpl>',
    '<tpl if="this.isPositiveStrand(values.strand)">',
    '" fill="green" opacity="0.8"/>',
    '<tpl else>',
    '" fill="red" opacity="0.8"/>',
    '</tpl>',
    '</svg>',
    '</td></tr>',
    '</table>',
    {
        disableFormats: true,
        getPctg: function (pos, length, strand) {

            var posicion = pos;
            if (strand === "-") {
                posicion = length - posicion + 1;
            }

            return (posicion / length) * 100;
        },
        getPosInGenome: function (strand, pos, exons) {

            if (strand === "+") {

                var tamanyototal = 0;
                for (var i = 0; i < exons.length; i++) {

                    var exonLength = exons[i].end - exons[i].start + 1;
                    if (tamanyototal + exonLength > pos) // El binding está en este exón
                        return (pos - tamanyototal + exons[i].start);
                    tamanyototal += exonLength;
                }

            } else {

                var tamanyototal = 0;
                for (var i = 0; i < exons.length; i++) {

                    var exonLength = exons[i].end - exons[i].start + 1;
                    if (tamanyototal + exonLength > pos) // El binding está en este exón
                        return (exons[i].end - pos + tamanyototal);
                    tamanyototal += exonLength;
                }



            }

            return (1);
        },
        removeChr: function (chromosome) {
            return chromosome.replace('chr', '').replace(/ /g, '');
        },
        getExonPos: function (exons, cual, strand, longi) {

            var total = 0;
            var posiciones = [];

            if (strand === "+") {
                for (var i = 0; i < exons.length; i++) {
                    if (i % 2 === cual) {
                        posiciones.push({"izq": (total / longi) * 100, "width": ((total / longi) + (exons[i].end - exons[i].start + 1) / longi) * 100});
                    }
                    total += exons[i].end - exons[i].start + 1;
                }
            } else {
                for (var i = exons.length - 1; i >= 0; i--) {
                    if (i % 2 === cual) {
                        posiciones.push({"izq": (total / longi) * 100, "width": ((total / longi) + (exons[i].end - exons[i].start + 1) / longi) * 100});
                    }
                    total += exons[i].end - exons[i].start + 1;
                }
            }

            return posiciones;
        },
        isPositiveStrand: function (strand) {
            return strand === "+";
        }

    });
