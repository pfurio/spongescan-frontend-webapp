
Ext.define('SPONGE.controller.CreateAnalysisPanel', {
    extend: 'Ext.app.Controller',
    views: [
        'CreateAnalysisPanel'
    ],
    stores: [
        'EnsemblGTF',
        'EnsemblCDNA',
        'MiRBase'
    ],
    models: [
        'Ensembl'
    ],
    // My variables
    gtf_sel: "upload",
    fasta_sel: "upload",
    release: null,
    species: null,
    ens_gtf: null,
    ens_fasta: null,
    upload_gtf_filefield: null,
    upload_fasta_filefield: null,
    upload_expr_lncrna_filefield: null,
    upload_expr_mirna_filefield: null,
    use_cons: 0,
    cons_db: null,
    mail: null,
    lodscore: 1,
    kmer_complex: 4,
    sd: 10,
    total_sites: 20,
    wobbles: true,
    reverse_fasta: false,
    mirna_selection: null,
    init: function () {

        this.control({
            'CreateAnalysisPanel': {
                boxready: function () {
                    Ext.getCmp("release_gtf_combo").getStore().load({"params": {"release": "all"}});
                    Ext.getCmp("release_fasta_combo").getStore().load({"params": {"release": "all"}});
                }

            },
            // PARAMETERS
            'radiogroup[name="gtf_radiogroup"]': {
                change: function (thiss, newValue) {

                    this.gtf_sel = newValue.gtf_sel;

                    if (newValue.gtf_sel === "ensembl") {
                        // Hide upload form and show Ensembl combobox
                        thiss.next().down().next().hide();
                        thiss.next().down().show();
                    } else if (newValue.gtf_sel === "upload") {
                        // Hide Ensembl combobox and show upload form
                        thiss.next().down().hide();
                        thiss.next().down().next().show();
                    }
                }
            },
            // PARAMETERS
            'radiogroup[name="fasta_radiogroup"]': {
                change: function (thiss, newValue) {

                    this.fasta_sel = newValue.fasta_sel;

                    if (newValue.fasta_sel === "ensembl") {
                        // Hide upload form and show Ensembl combobox
                        thiss.next().down().next().hide();
                        thiss.next().down().show();
                    } else if (newValue.fasta_sel === "upload") {
                        // Hide Ensembl combobox and show upload form
                        thiss.next().down().hide();
                        thiss.next().down().next().show();
                    }
                }
            },
            'combobox[name="release_gtf"]': {
                change: function (thiss, newValue) {

                    thiss.next().clearValue();
                    if (newValue != null) {
                        thiss.next().getStore().load({"params": {"release": newValue, "species": "all"}});
                        thiss.next().setDisabled(false);

                        this.release = newValue;
                    } else {
                        thiss.next().getStore().removeAll();
                        thiss.next().setDisabled(true);
                    }

                    var myfasta_combo = Ext.getCmp("release_fasta_combo");
                    if (myfasta_combo.getValue() !== newValue )
                        myfasta_combo.select(newValue);
                }
            },
            'combobox[name="species_gtf"]': {
                change: function (thiss, newValue) {
                    thiss.next().clearValue();
                    if (newValue != null) {
                        thiss.next().getStore().load({"params": {"release": thiss.prev().getValue(), "species": newValue}});
                        thiss.next().setDisabled(false);
                        this.species = newValue;
                    } else {
                        thiss.next().getStore().removeAll();
                        thiss.next().setDisabled(true);
                    }

                    var myfasta_combo = Ext.getCmp("species_fasta_combo");
                    if (myfasta_combo.getValue() !== newValue )
                        myfasta_combo.select(newValue);


                }
            },
            'combobox[name="gtf"]': {
                change: function (thiss, newValue) {
                    this.ens_gtf = newValue;
		    x = Ext.getCmp("gtf_url_box");
		    if (newValue != null) {
			x.setHtml('<i class="fa fa-cloud-download" style="color: blue"></i> <a style="color: blue" href="' + thiss.selection.data.path + '" target="_blank">GTF Ensembl url</a>');
			x.show();
		    } else {
			x.hide();
		    }
                }
            },
            'combobox[name="release_fasta"]': {
                change: function (thiss, newValue) {

                    thiss.next().clearValue();
                    if (newValue != null) {
                        thiss.next().getStore().load({"params": {"release": newValue, "species": "all"}});
                        thiss.next().setDisabled(false);

                        this.release = newValue;
                    } else {
                        thiss.next().getStore().removeAll();
                        thiss.next().setDisabled(true);
                    }

                    var mygtf_combo = Ext.getCmp("release_gtf_combo");
                    if (mygtf_combo.getValue() !== newValue )
                        mygtf_combo.select(newValue);

                }
            },
            'combobox[name="species_fasta"]': {
                change: function (thiss, newValue) {
                    thiss.next().clearValue();
                    if (newValue != null) {
                        thiss.next().getStore().load({"params": {"release": thiss.prev().getValue(), "species": newValue}});
                        thiss.next().setDisabled(false);
                        this.species = newValue;
                    } else {
                        thiss.next().getStore().removeAll();
                        thiss.next().setDisabled(true);
                    }

                    var mygtf_combo = Ext.getCmp("species_gtf_combo");
                    if (mygtf_combo.getValue() !== newValue )
                        mygtf_combo.select(newValue);

                }
            },
            'combobox[name="fasta"]': {
                change: function (thiss, newValue) {
                    this.ens_fasta = newValue;
		    x = Ext.getCmp("fasta_url_box");
                    if (newValue != null) {
                        x.setHtml('<i class="fa fa-cloud-download" style="color: blue"></i> <a style="color: blue" href="' + thiss.selection.data.path + '" target="_blank">Fasta Ensembl url</a>');
                        x.show();
		    } else {
                        x.hide();
		    }
                }
            },
            'combobox[name="mirna_combo"]': {
                change: function(thiss,newValue) {
                    this.mirna_selection = newValue;
                }
            },
            'filefield[name="upload_gtf_filefield"]': {
                change: function (thiss) {
                    this.upload_gtf_filefield = thiss.fileInputEl.dom.files.item(0);
                }
            },
            'filefield[name="upload_fasta_filefield"]': {
                change: function (thiss) {
                    this.upload_fasta_filefield = thiss.fileInputEl.dom.files.item(0);
                }
            },
            'filefield[name="upload_expr_lncrna_filefield"]': {
                change: function (thiss) {
                    this.upload_expr_lncrna_filefield = thiss.fileInputEl.dom.files.item(0);
                }
            },
            'filefield[name="upload_expr_mirna_filefield"]': {
                change: function (thiss) {
                    this.upload_expr_mirna_filefield = thiss.fileInputEl.dom.files.item(0);
                }
            },
            'radiogroup[name="conservation_radio"]': {
                change: function (thiss, newValue) {
                    this.use_cons = newValue.use_cons;
                    if (newValue.use_cons === 1) {
                        thiss.next().setDisabled(false);
                    } else if (newValue.use_cons === 0) {
                        thiss.next().setDisabled(true);
                    }
                }
            },
            'combobox[name="cons_db"]': {
                change: function (thiss, newValue) {
                    this.cons_db = newValue;
                }
            },
            'textfield[name="mail"]': {
                blur: function (thiss) {
                    this.mail = thiss.getValue();
                }
            },
            // OPTIONAL PARAMETERS (PREDICTOR)
            'numberfield[name="lods"]': {
                change: function (thiss, newValue) {
                    this.lodscore = newValue;
                }
            },
            'numberfield[name="kmer_complex"]': {
                change: function (thiss, newValue) {
                    this.kmer_complex = newValue;
                }

            },
            'numberfield[name="sd"]': {
                change: function (thiss, newValue) {
                    this.sd = newValue;
                }
            },
            'numberfield[name="binding_sites"]': {
                change: function (thiss, newValue) {
                    this.total_sites = newValue;
                }
            },
            'checkbox[name="wobbles"]': {
                change: function (thiss, newValue) {
                    this.wobbles = newValue;
                }
            },
            'checkbox[name="reverse"]': {
                change: function (thiss, newValue) {
                    this.reverse_fasta = newValue;
                }
            },
            // LOAD EXAMPLE BUTTON
		'button[name="load_example"]': {
		    click: function () {
			var mipanel = Ext.getCmp("mainViewCenterPanel");
			mipanel.setLoading(true);
			Ext.getCmp("release_gtf_combo").setValue("release-82");
			setTimeout(function(){
				Ext.getCmp("species_gtf_combo").setValue("caenorhabditis_elegans");
				setTimeout(function(){
					Ext.getCmp("gtf_combo").setValue("Caenorhabditis_elegans.WBcel235.82.gtf.gz");
					Ext.getCmp("fasta_combo").setValue("Caenorhabditis_elegans.WBcel235.ncrna.fa.gz");
					mipanel.setLoading(false);
				    }, 1500);
			    }, 1500);
			Ext.getCmp("mirna_combo").setValue("Caenorhabditis Elegans");
			Ext.getCmp("gtf_radiogroup").setValue({gtf_sel: "ensembl"});
			Ext.getCmp("fasta_radiogroup").setValue({fasta_sel: "ensembl"});
		    }
		},
            // SUBMIT BUTTON
            'button[name="submit_button"]': {
                click: function () {
                    // Generate form to be sent
                    var mydata = new FormData();
                    mydata.append("gtf_sel", this.gtf_sel);
                    mydata.append("fasta_sel", this.fasta_sel);
                    mydata.append("release", this.release);
                    mydata.append("species", this.species);
                    mydata.append("ens_gtf", this.ens_gtf);
                    mydata.append("ens_fasta", this.ens_fasta);
                    mydata.append("upload_gtf_filefield", (this.gtf_sel === "upload" ? this.upload_gtf_filefield : null));
                    mydata.append("upload_fasta_filefield", (this.fasta_sel === "upload" ? this.upload_fasta_filefield : null));
                    mydata.append("upload_expr_lncrna_filefield", this.upload_expr_lncrna_filefield);
                    mydata.append("upload_expr_mirna_filefield", this.upload_expr_mirna_filefield);
                    mydata.append("use_cons", this.use_cons);
                    mydata.append("cons_db", this.cons_db);
                    mydata.append("lod_score", this.lodscore);
                    mydata.append("kmer_complex", this.kmer_complex);
                    mydata.append("sd", this.sd);
                    mydata.append("total_sites", this.total_sites);
                    mydata.append("wobbles", this.wobbles);
                    // If the fasta file is retrieved from Ensembl, set reverse fasta to True
                    mydata.append("reverse_fasta", (this.fasta_sel === "upload" ? this.reverse_fasta : true));
                    mydata.append("mail", this.mail);
                    mydata.append("mirnas", this.mirna_selection);
                    Ext.MessageBox.show({
                        title: 'Please wait',
                        msg: 'Uploading items... Do not refresh or close the web browser.',
                        progressText: 'Initializing...',
                        width: 400,
                        height: 100,
                        progress: true,
                        closable: false,
                        animateTarget: this
                    });
                    function progressHandlingFunction(e) {
                        if (e.lengthComputable) {
                            var pctj = e.loaded / e.total;
                            Ext.MessageBox.updateProgress(pctj, Math.round(pctj * 100) + '% completed');
                            if (pctj === 100) {
                                Ext.MessageBox.updateProgress(100, '100% completed');
                            }

                        }
                    }

		    var _this = this;
                    $.ajax({
                        xhr: function ()
                        {
                            var myXhr = $.ajaxSettings.xhr();
                            if (myXhr.upload) { // Check if upload property exists
                                myXhr.upload.addEventListener('progress', progressHandlingFunction, false); // For handling the progress of the upload
                            }
                            return myXhr;
                        },
                        url: '/webserver/submit',
                        type: "POST",
                        data: mydata,
                        dataType: 'json',
                        processData: false, // Don't process the files
                        contentType: false, // Set content type to false as jQuery will tell the server its a query string request

                        success: function (data) {
                            Ext.MessageBox.hide();
                            if (_this._mail === null || _this.mail === "") {
                                Ext.Msg.alert('Success', 'Your JOB ID is ' + data.jobId + '. Your job has been added to the queue.');
                            } else {
                                Ext.Msg.alert('Success', 'Your JOB ID is ' + data.jobId + '. We will notify you by e-mail when the analysis is finished.');
                            }
                        },
                        error: function (jqXHR, textStatus, error) {
                            Ext.MessageBox.hide();
                            try {
                                if (jqXHR.status === 0) {
                                    Ext.Msg.alert(error, 'Not connect.n Verify Network.');
                                } else if (jqXHR.status == 404) {
                                    Ext.Msg.alert(error, 'Requested page not found. [404]');
                                } else if (jqXHR.status == 500) {
                                    Ext.Msg.alert(error, 'Internal Server Error [500].');
                                } else if (jqXHR.status == 400) {
                                    Ext.Msg.alert(error, jqXHR.responseJSON.msg);
                                } else if (exception === 'parsererror') {
                                    Ext.Msg.alert(error, 'Requested JSON parse failed.');
                                } else if (exception === 'timeout') {
                                    Ext.Msg.alert(error, 'Time out error.');
                                } else if (exception === 'abort') {
                                    Ext.Msg.alert(error, 'Ajax request aborted.');
                                } else {
                                    Ext.Msg.alert(error, 'Unknown error');
                                }
                            } catch(err) {
                                Ext.Msg.alert(error, jqXHR.status + ': Unknown error');
                            }

                        }
                    });
                }
            }

        });

    }
});
