//MainView controller

var jobID = null;

Ext.define('SPONGE.controller.MainView', {
    extend: 'Ext.app.Controller',
    views: [
        'MainView',
        'HomePanel'
    ],
    currentView: null,
    subViews: null,
    init: function () {

        this.subViews = {};

        this.control({
            'mainView': {
                boxready: function () {
                    var me = this;

                    $(".menu a").click(function () {
                        me.changeMainView(this.name);
                    });

                    this.changeMainView("HomePanel");
                }
            }
        })

    },
    getViewName: function () {
        return this.name;
    },
    changeMainView: function (aViewName) {

        var aView = null;
        var me = this;
        if (aViewName === "") {
            return;
        }

        if (aViewName === "RetrieveAnalysisPanel") {
            Ext.MessageBox.prompt('Job Id', 'Please enter the Job Id:', function (btn, jobId) {
                if (btn === "ok" && jobId !== "") {

                    // Check if job_id exists...
                    $.ajax({
                        url: "webserver/jobStatus/" + jobId,
                        success: function () {

                            if (me.subViews["ResultsPanel"] === undefined) {
                                aView = Ext.widget({xtype: "ResultsPanel"});
                                me.subViews["ResultsPanel"] = aView;
                            }

			    jobID = jobId;
			   
			    $.ajax({
                                    url: '/webserver/getArguments/' + jobID,
					success: function (data) {
                                        var mytext = "";
					mytext += "<span class='paramsTitle'> Reference genome: </span><span class='paramsAttr'>" + data['fasta'] + "</span><br>";
					mytext += "<span class='paramsTitle'> Annotation file: </span><span class='paramsAttr'>" + data['gtf'] + "</span><br>";
					mytext += "<span class='paramsTitle'> miRNA database: </span><span class='paramsAttr'>" + data['mirna'] + "</span><br><br>";
					mytext += "<span class='paramsTitle'> LOD Score: </span><span class='paramsAttr'>" + data['lod_score'] + "</span><br>";
					mytext += "<span class='paramsTitle'> Standard Deviation: </span><span class='paramsAttr'>" + data['sd'] + "</span><br>";
					mytext += "<span class='paramsTitle'> Complexity Score: </span><span class='paramsAttr'>" + data['kmer_complex'] + "</span><br>";
					mytext += "<span class='paramsTitle'> Minimum number of sites: </span><span class='paramsAttr'>" + data['total_sites'] + "</span><br>";
					mytext += "<span class='paramsTitle'> Wobbles: </span><span class='paramsAttr'>" + data['wobbles'] + "</span><br>";
                    speciesName = data['mirna'];
                    runGenomeViewer();
					/* for (var i in data) {
                                            mytext += "<span style='font-weight: lighter; font-family: Roboto, serif; font-size: 15px; color: #5A5A5A;'>"
                                                + i + "</span>: " +
                                                "<span style='font-weight: lighter; font-family: Roboto, serif; font-size: 15px; color: #4057FA;'>"
                                                + data[i] + "</span><br>";
                                        }
					*/
					var paramPanel = Ext.getCmp("parametersPanel");
					paramPanel.setHtml(mytext);
					paramPanel.show();
					
                                    },
					error: function () {
					Ext.getCmp("parametersPanel").hide();
                                        console.log("The parameters could not be shown");
                                    }
                                });
			    
                            var miGrid = Ext.getCmp("resultsGrid");
                            var mistore = miGrid.getStore();

                            mistore.loadPage(1, {"url": "webserver/getJob/" + jobId + "/" + Ext.getCmp('kmer_combo').getValue() + "/" + 
                                (Ext.getCmp('mirna_checkbox').getValue() == true ? "1" : "0")});

                            // Update title
                            var title_grid = Ext.getCmp("grid_title");
			    //  title_grid.setHtml('<h2 class="pull-left">Job ID: ' + jobID + '</h2><h3 id="LoadParameters" class="pull-right">Parameters</h3>');
			    title_grid.setHtml('<h2>Job ID: ' + jobID + '</h2>');
                            var centerPanel = Ext.getCmp("mainViewCenterPanel");
                            // Remove previous view
                            centerPanel.remove(me.subViews[me.currentView], false);

                            // Update current view
                            me.currentView = "ResultsPanel";

                            // Add new view
                            centerPanel.add(me.subViews["ResultsPanel"]);

			    },
				error: function (data) {
				Ext.Msg.alert('Error', data.responseJSON.msg);
			    }
			});
		    
                } else {
                    Ext.Msg.alert('Error', "Cannot access the webservice");
                }
            });
        } else if (aViewName === "ResultsPanel") {

            if (this.subViews[aViewName] === undefined) {
                aView = Ext.widget({xtype: aViewName});
                this.subViews[aViewName] = aView;
            }

	    var miGrid = Ext.getCmp("resultsGrid");
	    var mistore = miGrid.getStore();
	    /*
            Ext.MessageBox.show({
                title: 'Which experiment do you want to see?',
                buttons: Ext.MessageBox.YESNO,
                buttonText: {
                    yes: "Human",
                    no: "Mouse"
                },
                scope: this,
                fn: function (btn) {
		    // Update title
		    */
                    var title_grid = Ext.getCmp("grid_title");

                    //if (btn === "yes") { //Human
                        jobID = "hsapiens";
                        //title_grid.setHtml('<h2 class="pull-left">Homo sapiens</h2>');
                        title_grid.setHtml('<h2 class="pull-left">Job ID: Homo sapiens</h2>');
			/*
		    } else { // Mouse                        
                        jobID = "mus_musculus";
                        //title_grid.setHtml('<h2 class="pull-left">Mus musculus</h2>');
                        title_grid.setHtml('<h2 class="pull-left">Job ID: Mus musculus</h2>');
                    }
			*/
		    $.ajax({
			    url: '/webserver/getArguments/' + jobID,
				success: function (data) {
				var mytext = "";
				mytext += "<span class='paramsTitle'> Reference genome: </span><span class='paramsAttr'>" + data['fasta'] + "</span><br>";
				mytext += "<span class='paramsTitle'> Annotation file: </span><span class='paramsAttr'>" + data['gtf'] + "</span><br>";
				mytext += "<span class='paramsTitle'> miRNA database: </span><span class='paramsAttr'>" + data['mirna'] + "</span><br><br>";
				mytext += "<span class='paramsTitle'> LOD Score: </span><span class='paramsAttr'>" + data['lod_score'] + "</span><br>";
				mytext += "<span class='paramsTitle'> Standard Deviation: </span><span class='paramsAttr'>" + data['sd'] + "</span><br>";
				mytext += "<span class='paramsTitle'> Complexity Score: </span><span class='paramsAttr'>" + data['kmer_complex'] + "</span><br>";
				mytext += "<span class='paramsTitle'> Minimum number of sites: </span><span class='paramsAttr'>" + data['total_sites'] + "</span><br>";
				mytext += "<span class='paramsTitle'> Wobbles: </span><span class='paramsAttr'>" + data['wobbles'] + "</span><br>";
                speciesName = data['mirna'];
                runGenomeViewer();
				/*
				for (var i in data) {
				    mytext += "<span style='font-weight: lighter; font-family: Roboto, serif; font-size: 15px; color: #5A5A5A;'>"
					+ i + "</span>: " +
					"<span style='font-weight: lighter; font-family: Roboto, serif; font-size: 15px; color: #4057FA;'>"
					+ data[i] + "</span><br>";
				}
				*/
				var paramPanel = Ext.getCmp("parametersPanel");
				paramPanel.setHtml(mytext);
				paramPanel.show();
			    },
				error: function () {
				Ext.getCmp("parametersPanel").hide();
				console.log("The parameters could not be shown");
			    }
			});
		    
                    mistore.loadPage(1, {"url": "webserver/getJob/" + jobID + "/" + Ext.getCmp('kmer_combo').getValue() + "/" + 
                                (Ext.getCmp('mirna_checkbox').getValue() == true ? "1" : "0")});
		    /*		    
                }
            });
		    */

        } else if (this.subViews[aViewName] === undefined) {
            aView = Ext.widget({xtype: aViewName});
            this.subViews[aViewName] = aView;
        }

        if (aViewName !== "RetrieveAnalysisPanel") {
            var centerPanel = Ext.getCmp("mainViewCenterPanel");
            // Remove previous view
            centerPanel.remove(this.subViews[me.currentView], false);

            // Update current view
            me.currentView = aViewName;

            // Add new view
            centerPanel.add(this.subViews[aViewName]);
        }
    }

});
