var myfilters = Ext.create('Ext.toolbar.Toolbar', {
    id: 'grid_filters',
    border: 'none',
    ui: 'footer',
    items: [{
        text: 'Show/Hide Fields',
        menu: Ext.create('Ext.menu.Menu', {
            id: 'mainMenu',
            items: [{
                xtype: 'checkboxgroup',
                id: 'selectedfields',
                columns: 3,
                vertical: true,
                width: 600,
                items: [
                    {boxLabel: 'Gene ID', name: 'gene_id', inputValue: 'gene_id', checked: false},
                    {boxLabel: 'Gene name', name: 'gene_name', inputValue: 'gene_name', checked: false},
                    {boxLabel: 'Transcript ID', name: 'trans_id', inputValue: 'trans_id', checked: false},
                    {boxLabel: 'Transcript name', name: 'trans_name', inputValue: 'trans_name', checked: true},
                    {boxLabel: 'Chromosome', name: 'chrom', inputValue: 'chrom', checked: false},
                    {boxLabel: 'Strand', name: 'strand', inputValue: 'strand', checked: false},
                    {boxLabel: 'Gene start', name: 'start', inputValue: 'start', checked: false},
                    {boxLabel: 'Gene end', name: 'end', inputValue: 'end', checked: false},
                    {boxLabel: 'Transcript start', name: 'trans_start', inputValue: 'trans_start', checked: false},
                    {boxLabel: 'Transcript end', name: 'trans_end', inputValue: 'trans_end', checked: false},
                    {boxLabel: 'Transcript length', name: 'length', inputValue: 'length', checked: false},
                    {boxLabel: 'Biotype', name: 'biotype', inputValue: 'biotype', checked: false},
                    {boxLabel: 'Kmer', name: 'kmer', inputValue: 'kmer', checked: true},
                    {boxLabel: 'miRNAs', name: 'mirnas', inputValue: 'mirnas', checked: true},
                    {boxLabel: 'LOD Score', name: 'lod_score', inputValue: 'lod_score', checked: true},
                    {boxLabel: 'Complexity Score', name: 'compl_score', inputValue: 'compl_score', checked: true},
                    {boxLabel: 'Window size', name: 'w_size', inputValue: 'w_size', checked: false},
                    {boxLabel: 'Occupancy within window', name: 'occ', inputValue: 'occ', checked: false},
                    {boxLabel: 'Total occupancy', name: 'total', inputValue: 'total', checked: false},
                    {boxLabel: 'Median of distance', name: 'median_distance', inputValue: 'median_distance', checked: false},
                    {boxLabel: 'Dispersity score', name: 'sd', inputValue: 'sd', checked: true},
                    {boxLabel: 'Graph', name: 'cons_graph', inputValue: 'cons_graph', checked: true},
                ]
            },
                {
                    xtype: 'button',
                    text: 'Refresh',
                    action: 'refreshFields'
                }
            ]
        })
    },
        {
            xtype: 'combobox',
            fieldLabel: 'Kmer size',
            store: Ext.create('Ext.data.Store', {
                fields: ['name'],
                data: [
                    {"name": '6'},
                    {"name": '7'},
                    {"name": '8'}

                ]
            }),
            queryMode: 'local',
            displayField: 'name',
            valueField: 'name',
            value: '6',
            width: 120,
            labelWidth: 65,
            editable: false,
            id: 'kmer_combo',
            name: 'kmer_combo'
        },
        {
            xtype: 'checkbox',
            boxLabel: 'Known miRNA',
            checked: true,
            id: 'mirna_checkbox',
            name: 'mirna_checkbox'
        },
        '->',
        {
            xtype: 'combobox',
            id: 'filterField',
            alias: 'filterField',
            fieldLabel: 'Filters',
            hideLabel: 'true',
            editable: false,
            store: Ext.create('Ext.data.Store', {
                fields: ['abbr', 'filter'],
                data: [
                    {filter: 'Gene ID', abbr: 'gene_id'},
                    {filter: 'Gene name', abbr: 'gene_name'},
                    {filter: 'Transcript ID', abbr: 'trans_id'},
                    {filter: 'Transcript name', abbr: 'trans_name'},
                    {filter: 'Chromosome', abbr: 'chrom'},
                    {filter: 'Strand', abbr: 'strand'},
                    {filter: 'Gene start', abbr: 'start'},
                    {filter: 'Gene end', abbr: 'end'},
                    {filter: 'Transcript start', abbr: 'trans_start'},
                    {filter: 'Transcript end', abbr: 'trans_end'},
                    {filter: 'Transcript length', abbr: 'length'},
                    {filter: 'Biotype', abbr: 'biotype'},
                    {filter: 'Kmer', abbr: 'kmer'},
                    {filter: 'miRNAs', abbr: 'mirnas'},
                    {filter: 'LOD Score', abbr: 'lod_score'},
                    {filter: 'Complexity Score', abbr: 'compl_score'},
                    {filter: 'Window size', abbr: 'w_size'},
                    {filter: 'Occupancy within window', abbr: 'occ'},
                    {filter: 'Total occupancy', abbr: 'total'},
                    {filter: 'Median of distance', abbr: 'median_distance'},
                    {filter: 'Dispersity score', abbr: 'sd'}
                ]
            }),
            valueField: 'abbr',
            displayField: 'filter',
            emptyText: 'Select filter fields...',
            //width: 180,
            listeners: {
                select: function (combo, records) {
                    var str = records.get('abbr');
                    if ((str === "start") || (str === "end") || (str === "trans_start") || (str === "trans_end") || (str === "w_size") || (str === "lod_score") ||
                        (str === "compl_score") || (str === "sd") || (str === "occ") || (str === "total") || (str === "median_distance") || (str === "length")) {
                        Ext.getCmp("filterOperation").setVisible(true);
                    }
                    else {
                        Ext.getCmp("filterOperation").setVisible(false);
                    }
                }
            }
        },
        {
            xtype: 'combobox',
            id: 'filterOperation',
            hideLabel: 'true',
            editable: false,
            hidden: true,
            store: Ext.create('Ext.data.Store', {
                fields: ['abbr', 'operation'],
                data: [
                    {abbr: 'lt', operation: '<'},
                    {abbr: 'gt', operation: '>'},
                    {abbr: 'eq', operation: '='}
                ]
            }),
            valueField: 'abbr',
            displayField: 'operation',
            emptyText: 'Op.',
            width: 50
        },
        {
            xtype: 'textfield',
            id: 'filterValue',
            hideLabel: 'true',
            emptyText: 'Enter text',
            name: 'filter_value',
            width: 100
        },
        {
            text: 'Add Filter',
            action: 'addFilter'
        }
    ]

});


Ext.define('SPONGE.view.ResultsPanel', {
    extend: 'Ext.container.Container',
    alias: 'widget.ResultsPanel',
    name: 'ResultsPanel',
    id: 'ResultsPanel',
    scrollable: true,
    layout: {type: 'vbox', align: 'stretch'},
    items: [{
        xtype: 'container',
        id: 'results_container',
        layout: {
            type: 'vbox',
            align: 'stretch'
        },
        /*flex: 1,*/
        padding: '20',
        items: [
            // {xtype: 'box', id: 'grid_title', html: '<h2>Your results</h2>'},
            {xtype: 'box', id: 'grid_title'},
            /*{
             xtype: 'box',
             id: 'tissuesSummary',
             html:
             '<div style="height:215px; width: 100%;">' +
             '<h3 style="margin:0;">Tissue summary</h3>' +
             '<div style="height:195px; overflow: hidden; width: 50%; float: right;" id="tissues_mirna_summary"></div>' +
             '<div style="height:195px; overflow: hidden; width: 50%;" id="tissues_lncrna_summary"></div>' +
             '</div>' +
             '<h3> Results </h3>',
             // $( "#tissuesSummary" ).hide( "slow", function(){ Ext.getCmp("tissuesSummary").setHidden(true);} );

             },*/
	    {xtype: 'fieldset', collapsible: true, title: 'Parameters', collapsed: true, id: 'parametersPanel', hidden: true, animCollapse: false, 
		    padding: '10 10 10 10', margin: '10 0 0 0', maxWidth: 400}, 
            {xtype: 'box', html: '<div>'
            + '<span style="float:left"><h3>Results</h3></span>'
            + '<a class="download" onclick="downloadCSV()">Download <i class="fa fa-file-excel-o"></i></a>'
            + '</div>'},
            /*{xtype: 'toolbar', ui: 'footer', border: 'none', items: [{xtype: org_cbox}, {xtype: kmer_cbox}, {xtype: seed_cbox}, {xtype: go_btn}]},*/
            myfilters,
            {xtype: 'toolbar', border: 'none', hidden: true, id: 'filterBar', ui: 'footer'},
            {xtype: 'gridpanel', id: 'resultsGrid', store: 'GridStore', myFilters: [], /*flex: 1*/ height: 600, columnLines: true,
                // Row expansion
                plugins: [{
                    ptype: 'rowexpander',
                    rowBodyTpl: new Ext.XTemplate(),
                    expandOnDblClick: false
                },
                    {ptype: 'gridfilters'}],
                columns: [
                    {header: 'Gene ID', dataIndex: 'gene_id', hidden: true, filter: {type: 'string', disabled: true}},
                    {header: 'Gene name', dataIndex: 'gene_name', hidden: true, filter: {type: 'string', disabled: true}},
                    {header: 'Transcript ID', dataIndex: 'trans_id', hidden: true, filter: {type: 'string', disabled: true}},
                    {header: 'Transcript name', dataIndex: 'trans_name', filter: {type: 'string', disabled: true}},
                    {header: 'Chromosome', dataIndex: 'chrom', hidden: true, filter: {type: 'string', disabled: true}},
                    {header: 'Strand', dataIndex: 'strand', hidden: true, filter: {type: 'string', disabled: true}},
                    {header: 'Gene start', dataIndex: 'start', hidden: true, filter: {type: 'number', disabled: true}},
                    {header: 'Gene end', dataIndex: 'end', hidden: true, filter: {type: 'number', disabled: true}},
                    {header: 'Transcript start', dataIndex: 'trans_start', hidden: true, filter: {type: 'number', disabled: true}},
                    {header: 'Transcript end', dataIndex: 'trans_end', hidden: true, filter: {type: 'number', disabled: true}},
                    {header: 'Transcript length', dataIndex: 'length', hidden: true, filter: {type: 'number', disabled: true}},
                    {header: 'Biotype', dataIndex: 'biotype', hidden: true, filter: {type: 'string', disabled: true}},
                    {header: 'Kmer', dataIndex: 'kmer', filter: {type: 'string', disabled: true}},
                    {header: 'miRNAs', dataIndex: 'mirnas', filter: {type: 'string', disabled: true}},
                    {header: 'LOD Score', dataIndex: 'lod_score', filter: {type: 'number', disabled: true}, renderer: Ext.util.Format.numberRenderer('0.000')},
                    {header: 'Complexity Score', dataIndex: 'compl_score', filter: {type: 'number', disabled: true}},
                    {header: 'Window size', dataIndex: 'w_size', hidden: true, filter: {type: 'number', disabled: true}},
                    {header: 'Occupancy within window', dataIndex: 'occ', hidden: true, filter: {type: 'number', disabled: true}},
                    {header: 'Total occupancy', dataIndex: 'total', hidden: true, filter: {type: 'number', disabled: true}},
                    {header: 'Median of distance', dataIndex: 'median_distance', hidden: true, filter: {type: 'number', disabled: true}},
                    {header: 'Dispersity score', dataIndex: 'sd', filter: {type: 'number', disabled: true}, renderer: Ext.util.Format.numberRenderer('0.000')},
                    {header: 'Graph', dataIndex: 'cons_graph', xtype: 'templatecolumn', flex: 0.35, tpl: tpl_graph}
                ]
            }
        ]
    }, {
        xtype: 'box',
        //title: 'Genome Viewer',
        id: 'genomeViewerPanel',
        //animCollapse: false,
        padding: '0 20 0 20',
        /*collapsible: true,
         collapsed: true,
         collapseDirection: 'bottom',
         maxHeight: 450,*/
        html: '<h3> Genome Viewer </h3> <div id="genomeviewer"></div>'
    }]
});
