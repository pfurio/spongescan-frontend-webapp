Ext.define('SPONGE.view.CreateAnalysisPanel', {
    extend: 'Ext.container.Container',
    alias: 'widget.CreateAnalysisPanel',
    name: "CreateAnalysisPanel",
    scrollable: true,
    layout: {
        type: 'vbox',
        align: 'top',
        padding: '30'
    },
    initComponent: function () {

        var me = this;
        me.border = 0;
        Ext.applyIf(me, {
            items: [
		    {xtype: "container", border: false, layout: {type: 'hbox'}, defaults: {width: 565}, items: [
																{xtype: 'box', html: '<h2> Submit new analysis </h2>', width: 250},
																{xtype: 'button', name: 'load_example', html: '<i class="fa fa-file-text-o" style="font-size: 18px;"></i> <span style="font-size: 15px; font-weight: bold;">Load Example</span>', width: 145, height: 40, margin: '0 0 0 165'},
		 ]},
                {xtype: "container", id: 'submit_form', border: false, layout: {type: 'vbox'}, defaults: {width: 565}, items: [
                    // GTF FILES !!
                    {xtype: 'box', html: '<h3> 1. Select or upload GTF file </h3>'},
                    {xtype: 'container', border: false, layout: {type: 'hbox'}, items: [
											{xtype: 'radiogroup', id: 'gtf_radiogroup', name: 'gtf_radiogroup', vertical: true, width: 150, columns: 1, defaults: {padding: '5'}, items: [
                            {boxLabel: 'Upload my GTF', name: 'gtf_sel', inputValue: 'upload', checked: true},
                            {boxLabel: 'Get from Ensembl', name: 'gtf_sel', inputValue: 'ensembl'}
                        ]
                        },
                        {xtype: 'container', border: false, padding: '5', layout: {type: 'vbox'}, items: [
                            // ENSEMBL COMBOBOX CONTAINING ALL GTF POSSIBILITIES
                            {xtype: 'container', buttonAlign: 'right', hidden: true, border: false, width: 450, items: [
                                {xtype: 'combobox', name: 'release_gtf', editable: false, id: 'release_gtf_combo', labelWidth: 60, width: 400, store: new SPONGE.store.EnsemblGTF(), queryMode: 'local',
                                    displayField: 'name', valueField: 'value', fieldLabel: 'Release'
                                },
                                {xtype: 'combobox', disabled: true, editable: false, name: 'species_gtf', id: 'species_gtf_combo', labelWidth: 60, width: 400, store: new SPONGE.store.EnsemblGTF(),
                                    queryMode: 'local', displayField: 'name', valueField: 'value', fieldLabel: 'Species'
                                },
                                {xtype: 'combobox', disabled: true, editable: false, name: 'gtf', id: 'gtf_combo', labelWidth: 60, width: 400, store: new SPONGE.store.EnsemblGTF(),
                                    queryMode: 'local', displayField: 'name', valueField: 'value', fieldLabel: 'GTF'}
                            ]},
                            {xtype: 'filefield', width: 400, name: 'upload_gtf_filefield', fieldLabel: 'GTF', labelWidth: 35, msgTarget: 'side', buttonText: 'Select file...'}
                        ]}
                    ]},
		    {xtype: 'container', border: false, layout: {type: 'hbox'}, items: [
			    {xtype: 'box', padding: '5 10 ', html: '<i class="fa fa-info-circle" style="color: blue"></i> <a style="color: blue" href="example/mygtf.gtf" target="_blank">See format example</a>'},
			    {xtype: 'box', id: 'gtf_url_box', padding: '5 10 ', hidden: true, html: ''}]},
                    // TRANSCRIPT FILES !!
                    {xtype: 'box', html: '<h3> 2. Upload your lncRNA transcript fasta file </h3>'},
                    {xtype: 'container', border: false, layout: {type: 'hbox'}, items: [
											{xtype: 'radiogroup', id: 'fasta_radiogroup', name: 'fasta_radiogroup', vertical: true, width: 150, columns: 1, defaults: {padding: '5'}, items: [
                            {boxLabel: 'Upload my FASTA', name: 'fasta_sel', inputValue: 'upload', checked: true},
                            {boxLabel: 'Get from Ensembl', name: 'fasta_sel', inputValue: 'ensembl'}
                        ]
                        },
                        {xtype: 'container', border: false, padding: '5', layout: {type: 'vbox'}, items: [
                            // ENSEMBL COMBOBOX CONTAINING ALL GTF POSSIBILITIES
                            {xtype: 'container', buttonAlign: 'right', hidden: true, border: false, width: 450, items: [
                                {xtype: 'combobox', name: 'release_fasta', editable: false, id: 'release_fasta_combo', labelWidth: 60, width: 400, store: new SPONGE.store.EnsemblGTF(), queryMode: 'local',
                                    displayField: 'name', valueField: 'value', fieldLabel: 'Release'
                                },
                                {xtype: 'combobox', disabled: true, editable: false, name: 'species_fasta', id: 'species_fasta_combo', labelWidth: 60, width: 400, store: new SPONGE.store.EnsemblGTF(),
                                    queryMode: 'local', displayField: 'name', valueField: 'value', fieldLabel: 'Species'
                                },
                                {xtype: 'combobox', disabled: true, editable: false, name: 'fasta', id: 'fasta_combo', labelWidth: 60, width: 400, store: new SPONGE.store.EnsemblCDNA(),
                                    queryMode: 'local', displayField: 'name', valueField: 'value', fieldLabel: 'FASTA'}
                            ]},
                            {xtype: 'filefield', width: 400, name: 'upload_fasta_filefield', fieldLabel: 'FASTA', labelWidth: 50, msgTarget: 'side', buttonText: 'Select file...'}
                        ]}
                    ]},
		    {xtype: 'container', border: false, layout: {type: 'hbox'}, items: [
			    {xtype: 'box', padding: '5 10 ', html: '<i class="fa fa-info-circle" style="color: blue"></i> <a style="color: blue" href="example/myfasta.fa" target="_blank">See format example</a>'},
			    {xtype: 'box', id: 'fasta_url_box', padding: '5 10 ', hidden: true, html: ''}]},
                    /*{xtype: 'container', border: false, layout: {type: 'hbox'}, items: [
                     // UPLOAD FORM
                     {xtype: 'filefield', padding: '5', width: 550, name: 'upload_fasta_filefield', fieldLabel: 'FASTA', labelWidth: 50, msgTarget: 'side', buttonText: 'Select file...'}
                     ]
                     },*/
                    // miRNAs section
                    {xtype: 'box', html: '<h3> 3. Select the miRNA species to compare to </h3>'},
                    {xtype: 'combobox', id: 'mirna_combo', name: 'mirna_combo', editable: false, labelWidth: 100, width: 555, store: new SPONGE.store.MiRBase(), queryMode: 'local', displayField: 'name', valueField: 'name', fieldLabel: 'miRNA species'},

                    {xtype: 'box', html: '<h3> 4. Upload your expression data (Optional) </h3>'},
                    {xtype: 'filefield', width: 555, name: 'upload_expr_lncrna_filefield', fieldLabel: 'lncRNA expression', labelWidth: 130, msgTarget: 'side', buttonText: 'Select file...'},
                    {xtype: 'filefield', width: 555, name: 'upload_expr_mirna_filefield', fieldLabel: 'miRNA expression', labelWidth: 130, msgTarget: 'side', buttonText: 'Select file...'},
		    {xtype: 'box', padding: '5 0 ', html: '<i class="fa fa-info-circle" style="color: blue"></i> <a style="color: blue" href="example/myexpression.txt" target="_blank">See format example</a>'},

		    /*
                    // Conservation analysis
                    {xtype: 'box', html: '<h3> 4. Conservation analysis (optional)</h3>'},
                    {xtype: 'container', disabled: true, border: false, layout: {type: 'hbox'}, items: [
                        {xtype: 'radiogroup', name: 'conservation_radio', vertical: true, width: 150, columns: 1, defaults: {padding: '5'}, items: [
                            {boxLabel: 'Yes', name: 'use_cons', inputValue: 1},
                            {boxLabel: 'No', name: 'use_cons', inputValue: 0, checked: true}
                        ]
                        },
                        // CONSERVATION COMBOBOX CONTAINING ALL POSSIBILITIES STORED IN THE DATABASE
                        {xtype: 'container', buttonAlign: 'right', disabled: true, border: false, width: 650, layout: {type: 'vbox', align: 'flex', padding: '5'}, items: [
                            {xtype: 'combobox', name: 'cons_db', width: 600, store: Ext.create('Ext.data.Store', {
                                fields: ['abbr', 'name'],
                                data: [
                                    {"abbr": "AL", "name": "Alabama"},
                                    {"abbr": "AK", "name": "Alaska"},
                                    {"abbr": "AZ", "name": "Arizona"}
                                ]}), queryMode: 'local', displayField: 'name', valueField: 'abbr', fieldLabel: 'Select conservation'}
                        ]}
                    ]},
		    */
                    {xtype: 'box', html: '<h3> 5. Insert your e-mail (Optional)</h3>'},
                    {xtype: 'textfield', width: 555, name: 'mail', fieldLabel: 'E-mail', labelWidth: 50, allowBlank: true},
                    {xtype: 'panel', width: 555, title: 'Optional arguments', margin: '20 0 0 0', layout: {type: 'hbox', align: 'stretch'}, items: [
                        {xtype: 'container', layout: {type: 'vbox', align: 'stretch'}, padding: 10, items: [
                            {xtype: 'numberfield', width: 170, name: 'lods', fieldLabel: 'LOD Score', labelWidth: 120, value: 1, maxValue: 10, minValue: 0.5,
                                beforeLabelTextTpl: '<i class="fa fa-question-circle" style="font-size: 15px; color:#0076E2;" data-qtip="This threshold will measure at which enrichment level the putative MREs are going to be reported (Default: >= 1)."></i> '},
                            {xtype: 'numberfield', width: 170, name: 'kmer_complex', fieldLabel: 'Kmer complexity', labelWidth: 120, value: 4, maxValue: 5, minValue: 0.5,
                                beforeLabelTextTpl: '<i class="fa fa-question-circle" style="font-size: 15px; color:#0076E2;" data-qtip="Measures the complexity of the kmer sequences. Kmers with a complexity score lower than the specified value' +
                                ' will be filtered out (Default: <= 4)."></i> '}
                        ]},
                        {xtype: 'container', layout: {type: 'vbox', align: 'stretch'}, padding: '10 10 10 40', items: [
                            {xtype: 'numberfield', width: 150, name: 'sd', fieldLabel: 'S. Deviation', labelWidth: 92, value: 10, maxValue: 30, minValue: 0.1,
                                beforeLabelTextTpl: '<i class="fa fa-question-circle" style="font-size: 15px; color:#0076E2;" data-qtip="Measures the' +
                                ' distance between binding sites. By default, kmer-lncRNA pairs with an standard deviation < 10 will be returned."></i> '},
                            {xtype: 'numberfield', width: 150, name: 'binding_sites', fieldLabel: 'Total sites', labelWidth: 90, value: 20, maxValue: 10000, minValue: 1,
                                beforeLabelTextTpl: '<i class="fa fa-question-circle" style="font-size: 15px; color:#0076E2;" data-qtip="Kmer-lncRNA pairs with a total number of binding sites lower than the specified value' +
                                ' will be discarded. Default: >=20"></i> '}
                        ]},
                        {xtype: 'container', layout: {type: 'vbox', align: 'stretch'}, padding: '10 10 10 40', items: [
                            {xtype: 'checkbox', width: 200, name: 'wobbles', boxLabel: 'Wobbles', checked: true, labelWidth: 10,
                                beforeSubTpl: '<i class="fa fa-question-circle" style="font-size: 15px; color:#0076E2;" data-qtip="If activated, it will allow for one wobble per kmer."></i> '},
                            {xtype: 'checkbox', width: 200, name: 'reverse', boxLabel: 'Fasta reverse', checked: false, labelWidth: 200,
                                beforeSubTpl: '<i class="fa fa-question-circle" style="font-size: 15px; color:#0076E2;" data-qtip="Must be activated when transcripts from the reverse strand are already complementary' +
                                ' reversed in the uploaded fasta file."></i> '}
                        ]}
                    ]},
                    {xtype: 'button', name: 'submit_button', html: '<i class="fa fa-send" style="font-size: 20px;"></i> <span style="font-size: 18px; font-weight: bold;">Send Job</span>', width: 145, margin: '20 5 5 5', height: 40}
                ]}
            ]
        });
        me.callParent(arguments);
    }
});
