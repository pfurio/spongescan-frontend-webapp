Ext.define('SPONGE.view.HomePanel', {
    extend: 'Ext.container.Container',
    alias: 'widget.HomePanel',
    name: "HomePanel",
    scrollable: true,
    padding: 20,

    initComponent: function () {

        var me = this;
        Ext.applyIf(me, {
            items: [
                {xtype: 'box',
                    html: '<div id="about">'
                    + '<h2> Welcome to spongeScan</h2>'
                    + '<h3> About spongeScan</h3>'
                    + '<p style="line-height: 18pt;"><b> spongeScan </b> is a web tool designed to identify and visualize lncRNAs acting as putative miRNA sponges, by searching for multiple miRNA binding sites in lncRNAs. </p>'
                    + '</div>'
                }, {xtype: "container", html: '<div id="home_panel">' + '<img src="img/pipeline_home.png" alt="Pipeline" style="width:800px;"></div>'}
            ]
        });
        me.callParent(arguments);
    }
});