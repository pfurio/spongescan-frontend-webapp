/**BC***************************************************************************
 * THIS FILE CONTAINS THE FOLLOWING COMPONENT DECLARATION
 * - MainView
 * - HomePanel
 **EC****************************************************************************/

Ext.define('SPONGE.view.MainView', {
    extend: 'Ext.container.Viewport',
    alias: 'widget.mainView',
    id: 'main',
    minWidth: 900,
    border: false,
    defaults: {border: 0},
    layout: "border",

    initComponent: function () {
        this.border = 0;
        //this.subViews = {};
        Ext.applyIf(this, {

            items: [
                {xtype: "box", region: 'north', style: "border-bottom: 4px solid #333 !important;background-color: #000; height: 100px;", html: '<div id="header">' +
                '<img src="img/sponge_logo_215x50.png" alt="lncScan" style="margin-top: 25px">' +
                '<span><img src="img/ebi_logo.png" alt="EMBL-EBI"><img src="img/cipf_logo.png" alt="CIPF"><img src="img/florida_logo.png" alt="UF"></span>'
                + '</div>'
                },
                {xtype: "box", margin: 0, padding: 0, width: 222, cls: "myDataLateralMenu", region: 'west',
                    html: "   <ul class='menu'>"
                    + "<span id='toolsWrapper' style='display:block'>"
                    + " <li class='parentOption'><a name='HomePanel'><i class='fa fa-home'></i>  Home</a> </li>"
                    + " <li class='parentOption'><a name='CreateAnalysisPanel'><i class='fa fa-send'></i>  Perform analysis</a> </li>"
                    + " <li class='parentOption'><a name='RetrieveAnalysisPanel'><i class='fa fa-folder-open'></i>  Retrieve analysis</a> </li>"
                    + " <li class='parentOption'><a name='ResultsPanel'><i class='fa fa-line-chart'></i>  Show precomputed results</a> </li>"
		    + " <li class='parentOption'><a href='http://spongescan.readthedocs.org/en/latest/Home/' target='_blank'><i class='fa fa-book'></i>  Show documentation</a> </li>"
                        //                            + " <li class='parentOption'><a name='HelpPanel'><i class='fa fa-life-ring'></i>  Help</a> </li>"
                    + "</ul>"
                },
                {xtype: 'panel', region: 'center', id: 'mainViewCenterPanel', itemId: 'mainViewCenterPanel', defaults: {border: 0}, layout: 'fit', items: []}
            ]
        });
        this.callParent(arguments);
    }
});
