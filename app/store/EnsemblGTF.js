Ext.define('SPONGE.store.EnsemblGTF', {
    extend: 'Ext.data.Store',
    model: 'SPONGE.model.Ensembl',
    pageSize: 0,
    autoLoad: false,
    proxy: {
        type: 'rest',
        url: '/webserver/getEnsemblGTFs',
        reader: {
            root: 'response',
            successProperty: 'success'
        }

    }
});
