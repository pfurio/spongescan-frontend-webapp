Ext.define('SPONGE.store.GridStore', {
	//extend: 'Ext.data.Store',
	extend: 'Ext.data.BufferedStore',
	model: 'SPONGE.model.GridModel',
	pageSize: 100,
	remoteSort: true,
	remoteFilter: true,
	proxy: {
		type: 'rest',
		reader: {
			root: 'response',
			successProperty: 'success',
			totalProperty: 'totalCounts'
		},
		simpleSortMode: true
	},
	sorters: [{
		property: 'lod_score',
		direction: 'DESC'
	}],
	listeners: {
		load: function (esto) {

			// Get the conservation score values
			// updatePhastCons(records);
			// Show message
		if (esto.totalCount == 0 && esto.getData().getCount() > 0) 
		    esto.load();
		else
		    Ext.example.msg(esto.totalCount + " results are shown", "");

			// Enable toolbars
			myfilters.setDisabled(false);
			Ext.getCmp('filterBar').setDisabled(false);

			if (genomeViewer !== null) {
				tracks[2].dataAdapter.templateVariables.jobID = jobID;
				tracks[2].dataAdapter.templateVariables.kmer_size = Ext.getCmp('kmer_combo').getValue();
			}
			/*
			if (genomeViewer === null) {
			    runGenomeViewer();
			}
			*/
			
			// Get the accessibily score values
			//updateAccessibility(records);

		},

		beforeLoad: function(esto) {
			if (jobID != null) {
				esto.proxy.setUrl("webserver/getJob/" + jobID + "/" + Ext.getCmp('kmer_combo').getValue() + "/"
					+ (Ext.getCmp('mirna_checkbox').getValue() == true ? "1" : "0"));
			} else {
			    return false;
			}

		}
	},
	autoLoad: true

});
