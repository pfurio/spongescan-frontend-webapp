Ext.define('SPONGE.store.EnsemblCDNA', {
    extend: 'Ext.data.Store',
    model: 'SPONGE.model.Ensembl',
    pageSize: 0,
    autoLoad: false,
    proxy: {
        type: 'rest',
        url: '/webserver/getEnsemblCDNAs',
        reader: {
            root: 'response',
            successProperty: 'success'
        }

    }
});
